package com.founder.apabi.mylibrary;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by niuxm on 2016/11/16.
 */

public class ToastUtil {
    public static void shortMsg(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }
}
