package com.founder.apabi.deemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.founder.apabi.mylibrary.ToastUtil;

public class MainActivity extends AppCompatActivity {

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ToastUtil.shortMsg(this,"Toast");

    }
}
