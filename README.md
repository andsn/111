# [Gradle](gradle.org)

## Configure Build Types
<pre>
buildTypes {
	release {
		minifyEnabled true
		proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.pro'
</pre>
 <http://google.github.io/android-gradle-dsl/current/com.android.build.gradle.internal.dsl.BuildType.html>

## Configure Product Flavors
 Creating product flavors is similar to creating build types: add them to the productFlavors {} block and configure the settings you want. The product flavors support the same properties as defaultConfig—this is because defaultConfig actually belongs to the ProductFlavor class. This means you can provide the base configuration for all flavors in the defaultConfig {} block, and each flavor can override any of these default values, such as the applicationId.

<pre>
android {
    ...
    defaultConfig {...}
    buildTypes {...}
    productFlavors {
        demo {
            applicationId "com.example.myapp.demo"
            versionName "1.0-demo"
        }
        full {
            applicationId "com.example.myapp.full"
            versionName "1.0-full"
        }
    }
}
</pre>


## Create Source Sets for Build Variants


## Build with source sets

### priority:
>1. src/demoDebug/ (build variant source set)
>2. src/debug/ (build type source set)
>3. src/demo/ (product flavor source set)
>4. src/main/ (main source set)

- All source code in the java/ directories are compiled together to generate a single output.
> Note: For a given build variant, Gradle throws a build error if it encounters two or more source set directories that have defined the same Java class. For example, when building a debug APK, you cannot define both src/debug/Utility.java and src/main/Utility.java. This is because Gradle looks at both these directories during the build process and throws a 'duplicate class' error. If you want different versions of Utility.java for different build types, you can have each build type define its own version of the file and not include it in the main/ source set.

- Manifests are merged together into a single manifest. Priority is given in the same order as the list above. That is, manifest settings for a build type override the manifest settings for a product flavor, and so on. To learn more, read about manifest merging.

- Similarly, files in the values/ directories are merged together. If two files share the same name, such as two strings.xml files, priority is given in the same order as the list above. That is, values defined in a file in the build type source set override the values defined in the same file in a product flavor, and so on.

- Resources in the res/ and asset/ directories are packaged together. If there are resources with the same name defined in two or more source sets, priority is given in the same order as the list above.

- Finally, Gradle gives resources and manifests included with library module dependencies the lowest priority when building the APK.

## Merge Multiple Manifest Files
<https://developer.android.com/studio/build/manifest-merge.html>
![](art/manifest-merger.png)
There are three basic types of manifest files that may be merged into each other, and their merge priorities are as follows (highest priority first):

1. **Manifest file for your build variant**
>If you have multiple source sets for your variant, their manifest priorities are as follows:

>1. Build variant manifest (such as src/demoDebug/)
>2. Build type manifest (such as src/debug/)
>3. Product flavor manifest (such as src/demo/)

 _If you're using flavor dimensions, the manifest priorities correspond to the order each dimension is listed in the flavorDimensions property (first is highest priority)._

2. **Main manifest file for the app module**
3. **Manifest file from an included library**

_If you have multiple libraries, their manifest priorities match the dependency order (the order they appear in your Gradle dependencies block)._

> **Note**: These are the same merge priorities for all source sets, as described in Build with source sets.

> **Important**: Build configurations from the build.gradle file override any corresponding attributes in the merged manifest file. For example, the minSdkVersion from the build.gradle file overrides the matching attribute in the <uses-sdk> manifest element. To avoid confusion, you should simply leave out the <uses-sdk> element and define these properties only in the build.gradle file. For more details, see Configure Your Build.
	
**TODO**
## Declare Dependencies

### Module dependencies
> compile project(':mylibrary')

### Remote binary dependencies
> compile 'com.android.support:appcompat-v7:25.0.0'

### Local binary dependencies
> compile fileTree(dir: 'libs', include: ['*.jar'])

<pre>
dependencies {
    // The 'compile' configuration tells Gradle to add the dependency to the
    // compilation classpath and include it in the final package.

    // Dependency on the "mylibrary" module from this project
    compile project(":mylibrary")

    // Remote binary dependency
    compile 'com.android.support:appcompat-v7:25.0.0'

    // Local binary dependency
    compile fileTree(dir: 'libs', include: ['*.jar'])
}

</pre>

## Configure dependencies
> **compile** Specifies a compile time dependency. Gradle adds dependencies with this configuration to both the classpath and your app’s APK. This is the default configuration.

> **apk**Specifies a runtime-only dependency that Gradle needs to package with your app’s APK. You can use this configuration with JAR binary dependencies, but not with other library module dependencies or AAR binary dependencies.

> **provided**
 Specifies a compile time dependency that Gradle does not package with your app’s APK. This helps reduce the size of your APK if the dependency is not required during runtime. You can use this configuration with JAR binary dependencies, but not with other library module dependencies or AAR binary dependencies.


## Configure Signing Settings


<pre>
android {
    ...
    defaultConfig {...}
    signingConfigs {
        release {
            storeFile file("myreleasekey.keystore")
            storePassword "password"
            keyAlias "MyReleaseKey"
            keyPassword "password"
        }
    }
    buildTypes {
        release {
            ...
            signingConfig signingConfigs.release
        }
    }
}
</pre>


### 将证书配置到环境变量中
<pre>
signingConfigs {
    release {
        storeFile file(System.getenv("KEYSTORE"))
        storePassword System.getenv("KEYSTORE_PASSWORD")
        keyAlias System.getenv("KEY_ALIAS")
        keyPassword System.getenv("KEY_PASSWORD")
    }
}
</pre>

<pre>
android {
    ...
    signingConfigs {
        release {
            // We can leave these in environment variables
            storeFile file(System.getenv("KEYSTORE"))
            keyAlias System.getenv("KEY_ALIAS")

            // These two lines make gradle believe that the signingConfigs
            // section is complete. Without them, tasks like installRelease
            // will not be available!
            storePassword "notYourRealPassword"
            keyPassword "notYourRealPassword"
        }
    }
    ...
}

task askForPasswords << {
    // Must create String because System.readPassword() returns char[]
    // (and assigning that below fails silently)
    def storePw = new String(System.console().readPassword("Keystore password: "))
    def keyPw  = new String(System.console().readPassword("Key password: "))

    android.signingConfigs.release.storePassword = storePw
    android.signingConfigs.release.keyPassword = keyPw
}

tasks.whenTaskAdded { theTask -> 
    if (theTask.name.equals("packageRelease")) {
        theTask.dependsOn "askForPasswords"
    }
}
</pre>

### 在编译过程中手动输入密码
<pre>
final Console console = System.console();
if (console != null) {

    // Building from console 
    signingConfigs {
        release {
            storeFile file(console.readLine("Enter keystore path: "))
            storePassword console.readLine("Enter keystore password: ")
            keyAlias console.readLine("Enter alias key: ")
            keyPassword console.readLine("Enter key password: ")
        }
    }

} else {

    // Building from IDE's "Run" button
    signingConfigs {
        release {

        }
    }
}
</pre>

### 创建keystore.properties文件
<pre>
  File signFile = rootProject.file('sign/keystore.properties')
    if (signFile.exists()) {
        Properties properties = new Properties()
        properties.load(new FileInputStream(signFile))
        signingConfigs {
            release {
                storeFile rootProject.file(properties['keystore'])
                storePassword properties['storePassword']
                keyAlias properties['keyAlias']
                keyPassword properties['keyPassword']
            }
        }
    }
</pre>








